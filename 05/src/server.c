#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>

// inet_ntoa
#include <arpa/inet.h>

#define SERVER_PORT 31472
#define MAX_PENDING 5
#define MAX_LINE 256

int main() {
    struct sockaddr_in sin;
    char buf[MAX_LINE];
    int len;
    int s, new_s;

    /* build address data structure */
    bzero((char *)&sin, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons(SERVER_PORT);

    /* setup passive open */
    if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
        perror("simplex-talk: socket");
        exit(1);
    }
    if ((bind(s, (struct sockaddr *)&sin, sizeof(sin))) < 0) {
        perror("simplex-talk: bind");
        exit(1);
    }
    listen(s, MAX_PENDING);

    /* wait for connection, then receive and print text */
    int pid = 0;
    while (1) {
        if ((new_s = accept(s, (struct sockaddr *)&sin, &len)) < 0) {
            perror("simplex-talk: accept");
            exit(1);
        }

        struct sockaddr_in foo;
        int size_foo = sizeof(foo), r = -1;
        r = getpeername(new_s, (struct sockaddr *)&foo, &size_foo);
        if (r == -1)
            printf("simplex-talk: getsockname\n");
        printf("Client's socket %s:%d\n", inet_ntoa(foo.sin_addr),
                ntohs(foo.sin_port));

        if ((pid = fork()) == 0) {
            close(s);
            while (len = recv(new_s, buf, sizeof(buf), 0)) {
                fputs(buf, stdout);
                send(new_s, buf, len, 0);
            }
            close(new_s);
            exit(0);
        } else
            close(new_s);
    }
}
