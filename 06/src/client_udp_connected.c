/*
    Simple udp client
    Silver Moon (m00n.silv3r@gmail.com)
*/
#include <stdio.h> //printf
#include <string.h> //memset
#include <stdlib.h> //exit(0);
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>

#define BUFLEN 256  //Max length of buffer
#define PORT 31472   //The port on which to send data

void die(char *s)
{
    perror(s);
    exit(1);
}

int main(int argc, char **argv)
{
    struct sockaddr_in sin;
    struct hostent *hp;
    int s, i, slen=sizeof(sin);
    char buf[BUFLEN];
    char message[BUFLEN];
    char *host;

    if (argc == 2) {
        host = argv[1];
    } else {
        fprintf(stderr, "usage: ./client host\n");
        exit(1);
    }

    /* translate host name into peerâ€™s IP address */
    hp = gethostbyname(host);
    if (!hp) {
        fprintf(stderr, "simplex-talk: unknown host: %s\n", host);
        exit(1);
    }

    bzero((char *) &sin, sizeof(sin));
    sin.sin_family = AF_INET;
    bcopy(hp->h_addr_list[0], (char *)&sin.sin_addr, hp->h_length);
    sin.sin_port = htons(PORT);

    if ( (s=socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        die("socket");
    }

    if (connect(s, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    {
        die("simplex-talk: connect");
    }



    while(1)
    {
        printf("Enter message : ");
        gets(message);

        //send the message
        if (send(s, message, strlen(message) , 0) == -1)
        {
            die("send()");
        }

        //print details of the client/peer and the data received
        //printf("Received packet from %s:%d\n", inet_ntoa(sin.sin_addr), ntohs(sin.sin_port));
        //printf("Data: %s\n" , message);

        //receive a reply and print it
        //clear the buffer by filling null, it might have previously received data
        bzero(buf, BUFLEN);
        //try to receive some data, this is a blocking call
        if (recv(s, buf, BUFLEN, 0) == -1)
        {
            die("recv()");
        }

        puts(buf);
    }

    close(s);
    return 0;
}
