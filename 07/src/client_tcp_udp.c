/*
    Simple tcp & udp echo client
*/
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>

#define BUFLEN 512   // Max length of buffer
#define PORT 31472   // The port on which to send data

void die(char *s) {
    perror(s);
    exit(1);
}

int main(int argc, char **argv) {
    struct sockaddr_in sin;
    struct hostent *hp;
    int s;
    char buf[BUFLEN];
    char message[BUFLEN];
    char *host, *protocol;

    if (argc == 3) {
        protocol = argv[1];
        host = argv[2];
    } else {
        fprintf(stderr, "usage: ./client [TCP|UDP] host\n");
        exit(1);
    }

    // Translate host name into peers IP address
    if (!(hp = gethostbyname(host)))
        die("unknown host");

    bzero((char *) &sin, sizeof(sin));
    sin.sin_family = AF_INET;
    bcopy(hp->h_addr_list[0], (char *)&sin.sin_addr, hp->h_length);
    sin.sin_port = htons(PORT);

    if (strcmp("TCP", protocol) == 0) {
        if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0)
            die("socket");
    } else if (strcmp("UDP", protocol) == 0) {
        if ((s = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
            die("socket");
    } else {
        fprintf(stderr, "invalid protocol name\n");
        fprintf(stderr, "usage: ./client [TCP|UDP] host\n");
        exit(1);
    }

    if (connect(s, (struct sockaddr *)&sin, sizeof(sin)) < 0)
        die("connect");

    for (printf("Message: "); fgets(message, BUFLEN, stdin); printf("Message: ")) {
        // Send the message
        if (send(s, message, strlen(message) , 0) < 0)
            die("send");

        // Receive a reply and print it
        bzero(buf, BUFLEN);  // Clear buffer
        if (recv(s, buf, BUFLEN, 0) < 0)
            die("recv");

        printf("Received: %s\n", buf);
    }

    close(s);

    return 0;
}
