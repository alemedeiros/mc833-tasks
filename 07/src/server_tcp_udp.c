/*
    Simple tcp & udp echo server
*/
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <unistd.h>

#define BUFLEN 512   // Max length of buffer
#define PORT 31472   // The port on which to listen for incoming data
#define MAX_PENDING 5

void die(char *s) {
    perror(s);
    exit(1);
}

int main(void) {
    struct sockaddr_in serv, cli;

    int maxfd, nready;
    int len;
    socklen_t clen = sizeof(cli);
    fd_set rset, allset;
    int s_udp, s_tcp;
    char buf[BUFLEN];

    // Create a UDP socket
    if ((s_udp = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        die("UDP socket");

    // Create a TCP socket
    if ((s_tcp = socket(PF_INET, SOCK_STREAM, 0)) < 0)
        die("TCP socket");

    // Zero out the structure
    bzero((char *) &serv, sizeof(serv));
    serv.sin_family = AF_INET;
    serv.sin_port = htons(PORT);
    serv.sin_addr.s_addr = htonl(INADDR_ANY);  // TODO: check if htonl is only for UDP

    // Set socket options
    if (setsockopt(s_tcp, SOL_SOCKET, SO_REUSEADDR, &(int) {1}, sizeof(int)) ||
        setsockopt(s_udp, SOL_SOCKET, SO_REUSEADDR, &(int) {1}, sizeof(int)))
        die("SO_REUSEADDR failed");

    // Bind sockets to port
    if (bind(s_udp , (struct sockaddr*) &serv, sizeof(serv)))
        die("UDP socket bind");
    if (bind(s_tcp , (struct sockaddr*) &serv, sizeof(serv)))
        die("TCP socket bind");

    // Set TCP socket to listen
    if (listen(s_tcp, MAX_PENDING))
        die("listen");

    // Set fd sets
    maxfd = (s_udp > s_tcp) ? s_udp + 1 : s_tcp + 1;
    FD_ZERO(&allset);
    FD_SET(s_udp, &allset);
    FD_SET(s_tcp, &allset);

    // Use select to listen to sockets
    for ( ; ; ) {
        bzero((char *) &buf, BUFLEN);  // Clear buffer
        printf("Waiting for connection or data...\n");

        rset = allset;
        if ((nready = select(maxfd, &rset, NULL, NULL, NULL)) < 0)
            die("select");

        // New TCP connection
        if (FD_ISSET(s_tcp, &rset)) {
            int new_s;
            // Estabilish connection socket
            if ((new_s = accept(s_tcp, (struct sockaddr *) &cli, &clen)) < 0)
                die("TCP accept");

            // Fork to deal with new connection and wait for other connections
            if (fork() == 0) {
                close(s_udp);
                close(s_tcp);

                // Print details of the client
                struct sockaddr_in new_cli;
                socklen_t new_clen = sizeof(new_cli);
                if (getpeername(new_s, (struct sockaddr *)&new_cli, &new_clen))
                    die("TCP getsockname");
                printf("TCP connection estabilished with %s:%d\n", inet_ntoa(new_cli.sin_addr), ntohs(new_cli.sin_port));

                // Receive data from connection
                while ((len = recv(new_s, buf, sizeof(buf), 0))) {
                    printf("Received TCP packet from %s:%d\n", inet_ntoa(new_cli.sin_addr), ntohs(new_cli.sin_port));
                    printf("Data: %s" , buf);

                    if (send(new_s, buf, len, 0) < 0)
                        die("TCP send");

                    bzero((char *) &buf, BUFLEN);  // Clear buffer
                }

                close(new_s);
                exit(0);
            } else
                close(new_s);
        }

        // New UDP data
        if (FD_ISSET(s_udp, &rset)) {
            // Receive UDP data
            if ((len = recvfrom(s_udp, buf, BUFLEN, 0, (struct sockaddr *) &cli, &clen)) < 0)
                die("UDP recvfrom");


            // Print details of the client and the data received
            printf("Received UDP packet from %s:%d\n", inet_ntoa(cli.sin_addr), ntohs(cli.sin_port));
            printf("Data: %s" , buf);

            // Echo data
            if (sendto(s_udp, buf, len, 0, (struct sockaddr*) &cli, clen) < 0)
                die("UDP sendto");
        }
    }

    close(s_tcp);
    close(s_udp);
    return 0;
}
