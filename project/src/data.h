#ifndef DATA_H
#define DATA_H

#include "messages.pb.h"
#include <queue>
#include <list>
#include <string>
#include <mutex>
#include <condition_variable>

class User{

  private:
    int _socket;
    std::string _username;
    std::queue<chat::Response> _msg_queue;
    std::mutex m;
    std::condition_variable c;

  public:
    User() {}
    User(const std::string&, const int);
    User(const User& other);
    User& operator=(const User&);
    bool is_online();
    const std::string& get_username();
    int get_socket();
    void set_online(int);
    void set_offline();
    void push_msg_queue(const chat::Response&);
    const chat::Response pop_msg_queue();
    bool queue_empty();

};

class Group{

  private:
    std::string _groupname;
    std::list<std::string> _members;
    std::mutex addm_mutex;

  public:
    Group() {}
    Group(const std::string&);
    Group(const Group&);
    Group& operator=( const Group&);
    const std::string& get_groupname();
    void add_member(const std::string&);
    const std::list<std::string>& get_members();
    int size();

};


class Board{

  private:
    std::vector<std::vector<int> > _board;
    std::string _X, _O;
    std::string _last_player;
    bool win();
    bool draw();
    bool allowed_move(int, int);
    int playertoint(std::string);

  public:
    Board() {}
    Board(std::string, std::string);
    Board(const Board&);
    Board& operator=( const Board&);
    int make_move(const int, const int, const std::string);
    int is_filled(const int, const int);
    const std::string board_string();

};
#endif
