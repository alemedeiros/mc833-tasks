#define LISTENQ 100 // SOCKETS LISTENING
#define BUFFER_SIZE 1024 //MAX bytes per line of input
#define SERV_PORT 31472 //Port the server is running at

//COMMAND VALUES
#define dSEND 0
#define dCREATEG 1
#define dJOING 2
#define dGAME 3
#define dWHO 4
#define dEXIT 5
#define dSENDF 6
#define dSENDG 7
#define dGAMEA 8
#define dAUTH 9

//REPONSE VALUES
#define dACKNOLEDGE 0
#define dMESSAGE 1
#define dUSERS 2
//#define GAME 3;
#define dFILE 4

//STATUS VALUES
#define dERROR 0
#define dOK 1
#define dMESSAGE_RECEIVED 2
#define dMESSAGE_DELIVERED 3
