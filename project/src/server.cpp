/*
 * Code from http://www.unpbook.com/
 *
 *
 */

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <thread>
#include "values.h"
#include "chat.h"

#define LISTENQ 100 // SOCKETS LISTENING
#define BUFFER_SIZE 1024 //MAX bytes per line of input
#define SERV_PORT 31472 //Port the server is running at

int main(int argc, char *argv[]) {
    int listenfd, connfd;
    socklen_t clilen;
    struct sockaddr_in cliaddr, servaddr;
    int port;

    // Check arguments
    if (argc == 2) {
        port = atoi(argv[1]);
    } else {
        std::cerr << "usage: ./server port" << std::endl;
        exit(1);
    }

    if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket error");
        return 1;
    }

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(port);

    if (bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0)  {
        perror("bind error");
        close(listenfd);
        return 1;
    }

    if (listen(listenfd, LISTENQ) < 0) {
        perror("listen error");
        close(listenfd);
        return 1;
    }

    if(listenfd < 0 || listenfd >= FD_SETSIZE){
      perror("invalid file descriptor");
      exit(1);
    }

    std::cout << "=========== waiting for connections " << std::endl;
    while (1) {
      //Fica na espra bloqueante de um cliente
      //Isso garante conectar com um cliente por vez.
      if((connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &clilen)) < 0) {
          std::cout << "connfd: " << connfd << std::endl;
        perror("accept error");
        exit(1);
      }
      else if(listenfd >= 0 && listenfd < FD_SETSIZE){
        std::thread(auth, connfd).detach();
      }
      else{
        perror("invalid file descriptor");
        exit(1);
      }
    }
}
