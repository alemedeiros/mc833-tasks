#include <arpa/inet.h>
#include <cstdlib>
#include <errno.h>
#include <fstream>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <openssl/sha.h>
#include <queue>
#include <regex>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <thread>
#include <unistd.h>

#include "messages.pb.h"

#define BUFFER_SIZE 1024

namespace chat {
    // Define commands regexes (matching is case insensitive)
    // Quick reminder:
    //  - \S: non whitespace character
    //  - .: non EOL
    //  - [a-cA-C]: A to C (inclusive)
    //  - [1-3]: 1 to 3 (inclusive)
    //  - [^/]: not '/' character
    const std::regex e_exit ("exit", std::regex::icase);
    const std::regex e_who ("who", std::regex::icase);
    const std::regex e_createg ("createg (\\S+)", std::regex::icase);
    const std::regex e_joing ("joing (\\S+)", std::regex::icase);
    const std::regex e_game ("game (\\S+) ([a-c]) ([1-3])", std::regex::icase);
    const std::regex e_gamea ("gamea (\\S+) ([a-c]) ([1-3])", std::regex::icase);
    const std::regex e_send ("send (\\S+) (.+)", std::regex::icase);
    const std::regex e_sendg ("sendg (\\S+) (.+)", std::regex::icase);
    const std::regex e_sendf ("sendf (\\S+) (.+)", std::regex::icase);
    const std::regex e_fname ("(?:[^/]*/)*([^/]+)", std::regex::icase);

    bool authenticated = false;
    std::queue<Response> q;

    // Response handler function signature
    void handle_ack(const Response&);
    void handle_msg(const Response&);
    void handle_who(const Response&);
    void handle_game(const Response&);
    void handle_file(const Response&);

    // A separate namespace for functions that interact with server
    namespace web {
        // Wait for an response from the server
        const Response wait_on_response(const int socket) {
            char *buffer;
            int recvd, sz;

            // Get size of message
            /* std::cout << "=========== (wait_on_response) receiving " << std::endl; */
            recvd = recv(socket, &sz, sizeof(int), 0);
            if (recvd <= 0 || sz < 0) {
                std::cerr << "lost connection with server" << std::endl;
                exit(1);
            }
            /* std::cout << "=========== (wait_on_response) msg size " << sz << std::endl; */

            // Receive Response
            buffer = new char[sz];
            bzero(buffer, sz);
            recvd = recv(socket, buffer, sz, 0);
            if (recvd <= 0 || sz < 0) {
                std::cerr << "lost connection with server" << std::endl;
                exit(1);
            }
            /* std::cout << "=========== (wait_on_response) recvd: " << recvd << "/" << sz << std::endl; */

            // Parse protobuf
            Response r;
            r.ParseFromArray(buffer, sz);
            delete[] buffer;

            /* std::cout << "=========== (wait_on_response) response received" << std::endl; */
            /* r.PrintDebugString(); */

            return r;
        }

        // Function for thread that deals with responses. Make blocking call to
        // read from socket, and enqueues messages received.
        void receive_response(const int socket) {
            for ( ; ; ) {
                const Response& r = wait_on_response(socket);
                q.push(r);
            }
        }

        // Read responses from queue (non-blocking)
        void get_responses() {
            while (!q.empty()) {
                const Response r = q.front();
                q.pop();

                switch (r.type()) {
                    case Response::ACKNOWLEDGE:
                        handle_ack(r);
                        break;
                    case Response::MESSAGE:
                        handle_msg(r);
                        break;
                    case Response::USERS:
                        handle_who(r);
                        break;
                    case Response::GAME:
                        handle_game(r);
                        break;
                    case Response::FILE:
                        handle_file(r);
                        break;
                    default:
                        std::cerr << "unhandled response" << std::endl;
                }
            }
        }

        // Send command to server through socket
        void send_command(const int socket, const Command& command) {

            /* std::cout << "=========== (send_command) sending " << std::endl; */
            /* command.PrintDebugString(); */

            // Send size of message
            int sz = command.ByteSize();
            /* std::cout << "=========== (send_command) msg size " << sz << std::endl; */
            send(socket, &sz, sizeof(int), 0);

            // Send message itself
            char *buffer = new char[sz];
            command.SerializeToArray(buffer, sz);
            send(socket, buffer, sz, 0);
            delete[] buffer;
            /* std::cout << "=========== (send_command) sent: " << sent << std::endl; */
        }

        // Send an exit command for the user
        void exit(const int socket, const std::string& username) {
            Command command;
            command.set_cmd(Command::EXIT);
            command.set_src(username);

            send_command(socket, command);
        }

        // Send an authentication command for the user
        void auth(const int socket, const std::string& username) {
            Command command;
            command.set_cmd(Command::AUTH);
            command.set_src(username);

            send_command(socket, command);

            // Block until server responds
            Response r = wait_on_response(socket);

            // Check for server acknoledgement
            // We got to keep track of the ids somehow or don't check it
            authenticated = r.type() == Response::ACKNOWLEDGE
                && r.status() == Response::OK;
        }
    }

    // Return prompt string
    const std::string prompt(const std::string& username) {
        return "$[" + username + "] ";
    }

    // Print error message and exits with error
    void die(const std::string& str) {
        std::cerr << str << " (errno: " << strerror(errno) << ")" << std::endl;
        exit(1);
    }

    // Print help on commands
    void print_help() {
        // TODO(alemedeiros): help text
        std::cout << "TODO: help text" << std::endl;
    }

    // This assumes the id is a SHA512
    const std::string id_to_string(const unsigned char* id) {
        char buffer[SHA512_DIGEST_LENGTH * 2 + 1];
        for (int i = 0; i < SHA512_DIGEST_LENGTH / 4; ++i)
            sprintf(&buffer[i*2], "%02x", (unsigned int) id[i]);

        return std::string (buffer);
    }

    // Generate an ID for the message
    std::string gen_id(const std::string& msg) {
        char id[SHA512_DIGEST_LENGTH];
        SHA512((unsigned char*) msg.c_str(), msg.length(), (unsigned char*) id);

        return std::string (id, SHA512_DIGEST_LENGTH / 4);
    }

    int convert_x_axis(const std::string& x) {
        switch (x[0]) {
            case '3':
                return 0;
            case '2':
                return 1;
            case '1':
                return 2;
            default:
                return -1;
        }
    }

    int convert_y_axis(const std::string& y) {
        switch (y[0]) {
            case 'a':
            case 'A':
                return 0;
            case 'b':
            case 'B':
                return 1;
            case 'c':
            case 'C':
                return 2;
            default:
                return -1;
        }
    }

    void handle_ack(const Response& r) {
        std::string msg;

        switch (r.status()) {
            case Response::ERROR:
                msg = "ERROR: " + r.msg();
                break;
            case Response::MESSAGE_ENQUEUED:
                msg = "message " + id_to_string((unsigned char*) r.id().data());
                msg += " to " + r.sender() + " enqueued";
                break;
            case Response::MESSAGE_DELIVERED:
                msg = "message " + id_to_string((unsigned char*) r.id().data());
                msg += " to " + r.sender() + " delivered";
                break;
            case Response::GROUP_JOINED:
                msg = "group " + r.group_name() + " joined";
                break;
            case Response::GROUP_CREATED:
                msg = "group " + r.group_name() + " created";
                break;
            case Response::MOVE_ENQUEUED:
                msg = "move " + id_to_string((unsigned char*) r.id().data());
                msg += " on game with " + r.sender() + " enqueued";
                break;
            case Response::MOVE_DELIVERED:
                msg = "move " + id_to_string((unsigned char*) r.id().data());
                msg += " on game with " + r.sender() + " delivered";
                break;
            case Response::FILE_ENQUEUED:
                msg = "move " + id_to_string((unsigned char*) r.id().data());
                msg += " on game with " + r.sender() + " enqueued";
                break;
            case Response::FILE_DELIVERED:
                msg = "move " + id_to_string((unsigned char*) r.id().data());
                msg += " on game with " + r.sender() + " delivered";
                break;
            default:
                std::cerr << "unhandled ack resonse" << std::endl;
                return;
        }
        std::cout << msg << std::endl;
    }

    void handle_msg(const Response& r) {
        if (r.has_group_name())
            std::cout << "[" << r.group_name() << "] ";
        std::cout << r.sender() << ": " << r.msg() << std::endl;
    }

    void handle_who(const Response& r) {
        const int n = r.users_size();
        std::cout << n << " users:" << std::endl;

        // Print user list
        for (const Response::UserStatus& u : r.users()) {
            std::cout << "\t" << u.user() << "\t\t";
            if (u.online()) {
                std::cout << "online" << std::endl;
            } else {
                std::cout << "offline" << std::endl;
            }
        }
    }

    void handle_game(const Response& r) {
        std::cout << r.msg() << std::endl;
    }

    void handle_file(const Response& r) {
        std::smatch match;

        if (!std::regex_match(r.filename(), match, e_fname)) {
            std::cerr << "bad file name " << r.filename() << std::endl;
            return;
        }

        // Make sure directory exists
        const int err = system("mkdir -p ./downloads");
        if (err) {
            std::cerr << "unable to create downloads directory" << std::endl;
            return;
        }

        // Open file
        const std::string fname = "./downloads/" + match[1].str();
        std::ofstream file(fname, std::ios::binary | std::ios::ate);
        if (!file.good()) {
            std::cerr << "unable to open file " << fname << std::endl;
            return;
        }

        // Read file
        int size = r.file().size();
        if (file.write(r.file().data(), size)) {
            std::cout << "file " << fname << " just received from ";
            std::cout << r.sender() << std::endl;
        } else {
            std::cerr << "unable to write to file " << fname << std::endl;
        }
    }


    // Transform string line into a Command protobuf using regex
    const Command parse_command(const std::string& line,
            const std::string& username) {
        Command command;
        std::smatch match;

        // Set command issuer and message id
        command.set_src(username);
        command.set_id(gen_id(line));

        // Exit
        if (std::regex_match(line, e_exit)) {
            command.set_cmd(Command::EXIT);
        }

        // Who
        if (std::regex_match(line, e_who)) {
            command.set_cmd(Command::WHO);
        }

        // Create Group
        if (std::regex_match(line, match, e_createg)) {
            command.set_cmd(Command::CREATEG);
            command.set_dest(match[1]);
        }

        // Join Group
        if (std::regex_match(line, match, e_joing)) {
            command.set_cmd(Command::JOING);
            command.set_dest(match[1]);
        }

        // Start Game
        if (std::regex_match(line, match, e_game)) {
            command.set_cmd(Command::GAME);
            command.set_dest(match[1]);

            command.set_x_axis(convert_x_axis(match[3]));
            command.set_y_axis(convert_y_axis(match[2]));
        }

        // Play Game
        if (std::regex_match(line, match, e_gamea)) {
            command.set_cmd(Command::GAMEA);
            command.set_dest(match[1]);

            command.set_x_axis(convert_x_axis(match[3]));
            command.set_y_axis(convert_y_axis(match[2]));
        }

        // Send personal message
        if (std::regex_match(line, match, e_send)) {
            command.set_cmd(Command::SEND);
            command.set_dest(match[1]);
            command.set_msg(match[2]);
        }

        // Send group message
        if (std::regex_match(line, match, e_sendg)) {
            command.set_cmd(Command::SENDG);
            command.set_dest(match[1]);
            command.set_msg(match[2]);
        }

        // Send File
        if (std::regex_match(line, match, e_sendf)) {
            command.set_cmd(Command::SENDF);
            command.set_dest(match[1]);
            command.set_filename(match[2]);

            // Open file
            std::ifstream file(match[2], std::ios::binary | std::ios::ate);
            if (!file.good()) {
                // Reset protobuffer
                std::cerr << "unable to open file " << match[2] << std::endl;
                command.Clear();
                return command;
            }

            // Get file size
            std::streamsize size = file.tellg();
            char *buffer = new char[size];
            file.seekg(0, std::ios::beg);

            // Read file
            if (file.read(buffer, size)) {
                command.set_file(buffer, size);
            } else {
                // Reset protobuffer
                std::cerr << "unable to read file " << match[2] << std::endl;
                command.Clear();
            }
            delete[] buffer;
        }

        return command;
    }

    void run_client(const std::string& host, const int port,
            const std::string& username) {
        struct hostent *hp;
        struct sockaddr_in serv;
        int s;

        // Translate hostname into IP address
        hp = ::gethostbyname(host.c_str());
        if (!hp)
            die("unknown host");

        // Build address data structure
        bzero((char *) &serv, sizeof(serv));
        serv.sin_family = AF_INET;
        bcopy(hp->h_addr_list[0], (char *) &serv.sin_addr, hp->h_length);
        serv.sin_port = htons(port);

        // Open socket
        if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0)
            die("socket");

        // Connect to server
        /* std::cout << "=========== connecting! " << std::endl; */
        if (connect(s, (struct sockaddr *) &serv, sizeof(serv)) < 0) {
            close(s);
            die("connect");
        }

        struct sockaddr_in local;
        unsigned int size_local = sizeof(local);
        if ((getsockname(s, (struct sockaddr *) &local, &size_local)) == -1) {
            close(s);
            die("getsockname");
        }

        // Print socket port
        /* std::cout << "=========== Openned socket " << inet_ntoa(local.sin_addr) << ":"; */
        /* std::cout << local.sin_port << std::endl; */

        // Authenticate client
        /* std::cout << "=========== authenticating! " << std::endl; */
        web::auth(s, username);
        if (!authenticated) {
            close(s);
            die("failed to authenticate with server");
        }

        // Initialize receiver thread
        std::thread (web::receive_response, s).detach();

        // Main loop: get and send lines of text
        do {
            bool exit = false;
            std::string line;
            Command command;

            // Process any server responses
            web::get_responses();

            // Get command from stdin
            std::cout << chat::prompt(username) << " ";
            std::getline(std::cin, line);
            if (line.empty())
                continue;

            // Parse command
            command = parse_command(line, username);
            if (!command.IsInitialized()) {
                std::cerr << "Invalid command" << std::endl;
                print_help();
                continue;
            }

            // Execute command
            switch (command.cmd()) {
                case Command::SEND:
                case Command::CREATEG:
                case Command::JOING:
                case Command::SENDG:
                case Command::WHO:
                case Command::SENDF:
                case Command::GAME:
                case Command::GAMEA:
                    web::send_command(s, command);
                    break;

                case Command::EXIT:
                    exit = true;  // Just marks exit flag
                    break;

                default:
                    std::cerr << "Problem parsing command" << std::endl;
            }

            // Check for exit command
            if (exit)
                break;
        } while (!std::cin.eof());

        // Make sure client always exits with server
        web::exit(s, username);
    }
}

int main(int argc, char *argv[]) {
    char *host = nullptr, *username = nullptr;
    int port = -1;

    // Verify that the version of the library that we linked against is
    // compatible with the version of the headers we compiled against.
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    // Check arguments
    if (argc == 4) {
        host = argv[1];
        port = atoi(argv[2]);
        username = argv[3];
    } else {
        chat::die("usage: ./client host port username");
    }

    // Test if stuff is compiling
    chat::Command command;
    command.set_cmd(chat::Command::SEND);

    // Run client
    chat::run_client(host, port, username);

    // Delete all global objects allocated by libprotobuf.
    google::protobuf::ShutdownProtobufLibrary();
    return 0;
}
