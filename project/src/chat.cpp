#include "chat.h"


/******** AUXILIARY FUNCTIONS **********/
const chat::Command wait_on_command(const int, const std::string&);
void send_response(const int, const chat::Response&);
bool check_command(const chat::Command&, int);
/**************************************/

std::mutex auth_mutex;
std::map<std::string, User> users_map;
std::map<std::string, Group> group_map;
std::map<std::pair<std::string, std::string>, Board> tic_tac_toe;

/******** THREAD FUNCTIONS **********/
void auth(const int sockfd){
  std::lock_guard<std::mutex> lock(auth_mutex);
  std::cout << "=========== will authenticate! " << std::endl;

  chat::Command auth_cmd;
  chat::Response r;

  r.set_type(chat::Response::ACKNOWLEDGE);
  r.set_status(chat::Response::ERROR);
  auth_cmd = wait_on_command(sockfd, "");
  std::string user;

  std::cout << "=========== authenticating " << auth_cmd.src() << std::endl;

  if(check_command(auth_cmd, chat::Command::AUTH)){
      std::cout << "=========== auth received " << std::endl;
    user = auth_cmd.src();
    if (users_map.find(user) == users_map.end()) {
      std::cout << "=========== first time" << std::endl;
      // First authentication
      users_map[user] = User(user, sockfd);
      r.set_status(chat::Response::OK);
      r.set_id(auth_cmd.id());
    } else if(users_map[user].is_online()){
      std::cout << "=========== already logged user" << std::endl;
      // User is already logged in, refuse authentication to new client
      r.set_msg("user already logged in with another client");
    } else {
      std::cout << "=========== known user" << std::endl;
      // Otherwise is a known user authenticating
      users_map[user].set_online(sockfd);
      r.set_status(chat::Response::OK);
      r.set_id(auth_cmd.id());
    }
  } else {
    // Wrong command type!
      std::cout << "=========== not auth" << std::endl;
    r.set_msg("expected AUTH command");
  }

  send_response(sockfd, r);

  if(r.status() == chat::Response::OK){
    // If authentication occurred correctly, initialize listener and sender
    // threads
    std::thread(handle_command, user).detach();
    std::thread(handle_response, user).detach();
    std::cout << "=========== authenticated! " << std::endl;
  }
}


//Will keep on bloqued listening here
void handle_command(const std::string& user){
  User& my_user = users_map[user];

  while(my_user.is_online()){
    chat::Command command;
    command = wait_on_command(my_user.get_socket(), user);
    /* std::cout << "=========== command from " << user << std::endl; */
    /* command.PrintDebugString(); */

    switch (command.cmd()) {
      case chat::Command::WHO:
        if(check_command(command, chat::Command::WHO))
          who(user);
        break;
      case chat::Command::EXIT:
        if(check_command(command, chat::Command::EXIT)){
          exit(my_user.get_socket(), user);
        }
        break;
      case chat::Command::SEND:
        if(check_command(command, chat::Command::SEND)){
          send_msg(command);
        }
        break;
      case chat::Command::SENDG:
        if(check_command(command, chat::Command::SENDG)){
          send_group(command);
        }
        break;
      case chat::Command::SENDF:
        if(check_command(command, chat::Command::SENDF)){
          send_file(command);
        }
        break;
      case chat::Command::CREATEG:
        if(check_command(command, chat::Command::CREATEG)){
          create_group(command);
        }
        break;
      case chat::Command::JOING:
        if(check_command(command, chat::Command::JOING)){
          join_group(command);
        }
        break;
      case chat::Command::GAME:
        if(check_command(command, chat::Command::GAME)){
          game(command);
        }
        break;
      case chat::Command::GAMEA:
        if(check_command(command, chat::Command::GAMEA)){
          gamea(command);
        }
        break;
      default:
          std::cerr << "Problem parsing command" << std::endl;
    }
  }
}

void handle_response(const std::string& user){
  User& my_user = users_map[user];

  while (my_user.is_online()) {
    // pop_msg_queue waits on a condition variable.
    const chat::Response& r = my_user.pop_msg_queue();

    if (!r.IsInitialized())
        continue;

    std::cout << "=========== response to " << user << std::endl;

    send_response(my_user.get_socket(), r);

    std::cout << "=========== response sent" << std::endl;

    if (r.type() == chat::Response::USERS
            || r.type() == chat::Response::ACKNOWLEDGE
            || user == r.sender())
        continue;

    // Delivered ack to sender.
    chat::Response ack;
    ack.set_type(chat::Response::ACKNOWLEDGE);
    if (r.type() == chat::Response::MESSAGE) {
        ack.set_status(chat::Response::MESSAGE_DELIVERED);
    } else if (r.type() == chat::Response::FILE) {
        ack.set_status(chat::Response::FILE_DELIVERED);
    } else if (r.type() == chat::Response::GAME) {
        ack.set_status(chat::Response::MOVE_DELIVERED);
    } else {
        std::cerr << "unknown response type for ack" << std::endl;
        continue;
    }
    ack.set_id(r.id());
    ack.set_sender(user);
    users_map[r.sender()].push_msg_queue(ack);
  }
}
/**************************************/


void who(const std::string& user){
    chat::Response r;

    r.set_type(chat::Response::USERS);
    r.set_status(chat::Response::OK);

    // Use each pair of key value from the map to build response array.
    for (auto& p : users_map) {
        chat::Response::UserStatus* us = r.add_users();
        us->set_user(p.first);
        us->set_online(p.second.is_online());
    }

    users_map[user].push_msg_queue(r);
}

void exit(const int sockfd, const std::string& user){
  users_map[user].set_offline();
  close(sockfd);
}

void send_msg(const chat::Command& command) {

  // Enqueue Message to destination user
  chat::Response r;
  r.set_type(chat::Response::MESSAGE);
  r.set_status(chat::Response::OK);
  r.set_msg(command.msg());
  r.set_sender(command.src());
  r.set_id(command.id());

  // Enqueued ack to sender.
  chat::Response ack;
  ack.set_type(chat::Response::ACKNOWLEDGE);
  ack.set_status(chat::Response::MESSAGE_ENQUEUED);
  ack.set_id(command.id());
  ack.set_sender(command.dest());

  // Sends message to users
  users_map[command.src()].push_msg_queue(ack);
  users_map[command.dest()].push_msg_queue(r);
}

void send_group(const chat::Command& command) {

  chat::Response r;
  r.set_type(chat::Response::MESSAGE);
  r.set_status(chat::Response::OK);
  r.set_msg(command.msg());
  r.set_sender(command.src());
  r.set_group_name(command.dest());
  r.set_id(command.id());

  // Enqueued ack to sender.
  chat::Response ack;
  ack.set_type(chat::Response::ACKNOWLEDGE);
  ack.set_status(chat::Response::MESSAGE_ENQUEUED);
  ack.set_id(command.id());
  ack.set_sender(command.dest());
  users_map[command.src()].push_msg_queue(ack);

  // Send to each member
  for (const std::string& m : group_map[command.dest()].get_members()){
      users_map[m].push_msg_queue(r);
  }
}

void send_file(const chat::Command& command){

  chat::Response r;
  r.set_type(chat::Response::FILE);
  r.set_status(chat::Response::OK);
  r.set_file(command.file());
  r.set_filename(command.filename());
  r.set_sender(command.src());
  r.set_id(command.id());

  // Enqueued ack to sender.
  chat::Response ack;
  ack.set_type(chat::Response::ACKNOWLEDGE);
  ack.set_status(chat::Response::FILE_ENQUEUED);
  ack.set_id(command.id());
  ack.set_sender(command.dest());

  users_map[command.src()].push_msg_queue(ack);
  users_map[command.dest()].push_msg_queue(r);
}

void create_group(const chat::Command& command){

  group_map[command.dest()] = Group(command.dest());
  group_map[command.dest()].add_member(command.src());

  // Group Created ack to sender.
  chat::Response ack;
  ack.set_type(chat::Response::ACKNOWLEDGE);
  ack.set_status(chat::Response::GROUP_CREATED);
  ack.set_group_name(command.dest());
  users_map[command.src()].push_msg_queue(ack);
}

void join_group(const chat::Command& command){

  group_map[command.dest()].add_member(command.src());

  // Group Joined ack to sender.
  chat::Response ack;
  ack.set_type(chat::Response::ACKNOWLEDGE);
  ack.set_status(chat::Response::GROUP_JOINED);
  ack.set_group_name(command.dest());
  users_map[command.src()].push_msg_queue(ack);
}

void game(const chat::Command& command){

  std::pair<std::string, std::string> players(command.src(), command.dest());

  tic_tac_toe[players] = Board(command.src(), command.dest());
  int move = tic_tac_toe[players].make_move(command.x_axis(), command.y_axis(), command.src());

  if(move < 0){
    chat::Response err;

    // An illegal move SHOULD NOT have happened, at least not with the current
    // client... but lets be on the safe side and send this error message
    err.set_type(chat::Response::ACKNOWLEDGE);
    err.set_status(chat::Response::ERROR);
    err.set_msg("invalid move! Please try again");
    err.set_id(command.id());
    users_map[command.src()].push_msg_queue(err);

    // Make sure board is erased from map
    tic_tac_toe.erase(tic_tac_toe.find(players));

    return;
  }

  // Print Game message
  std::string game_msg = "   Game: " + command.src() + " vs. " + command.dest();
  game_msg += "\n\n" + command.src() + " just made a move!\n\n";
  game_msg += tic_tac_toe[players].board_string();
  game_msg += "\n\n  It's " + command.dest() + " turn!\n\n";

  // Build message responses
  chat::Response r;//, r_dest;
  r.set_type(chat::Response::GAME);
  r.set_status(chat::Response::OK);
  r.set_msg(game_msg);
  r.set_sender(command.src());
  r.set_id(command.id());

  // Game created ack to sender.
  chat::Response ack;
  ack.set_type(chat::Response::ACKNOWLEDGE);
  ack.set_status(chat::Response::MOVE_ENQUEUED);
  ack.set_id(command.id());
  ack.set_sender(command.dest());

  users_map[command.src()].push_msg_queue(ack);
  users_map[command.src()].push_msg_queue(r);
  users_map[command.dest()].push_msg_queue(r);
}

void gamea(const chat::Command& command){

  std::pair<std::string, std::string> players_opt1(command.dest(), command.src()), players_opt2(command.src(), command.dest());
  std::pair<std::string, std::string> players;

  // There can be only one game between two players (verified on check command)
  if(tic_tac_toe.find(players_opt1) != tic_tac_toe.end())
    players = players_opt1;
  else
    players = players_opt2;

  int move = tic_tac_toe[players].make_move(command.x_axis(), command.y_axis(), command.src());

  // Always print board
  std::string game_msg = "   Game: " + command.src() + " vs. " + command.dest();
  game_msg += "\n\n" + command.src() + " just made a move!\n\n";
  game_msg += tic_tac_toe[players].board_string();

  if(move < 0){
    chat::Response err;
    std::string err_msg;

    err_msg = "invalid move! Check the board and try again.\n\n" + tic_tac_toe[players].board_string();
    err.set_type(chat::Response::ACKNOWLEDGE);
    err.set_status(chat::Response::ERROR);
    err.set_msg(err_msg);
    err.set_id(command.id());
    users_map[command.src()].push_msg_queue(err);

    return;
  }

  // If Game is over, show the winner
  if(move == 3) {
    game_msg += "\n\n  It's a draw!!\n\n";
    tic_tac_toe.erase(tic_tac_toe.find(players));
  } else if (move > 0){
    game_msg += "\n\n  Player " + command.src() + " wins!!\n\n";
    tic_tac_toe.erase(tic_tac_toe.find(players));
  } else {
    game_msg += "\n\n  It's " + command.dest() + " turn!\n\n";
  }

  // Build message responses
  chat::Response r;
  r.set_type(chat::Response::GAME);
  r.set_status(chat::Response::OK);
  r.set_msg(game_msg);
  r.set_sender(command.src());
  r.set_id(command.id());

  // Game created ack to sender.
  chat::Response ack;
  ack.set_type(chat::Response::ACKNOWLEDGE);
  ack.set_status(chat::Response::MOVE_ENQUEUED);
  ack.set_id(command.id());
  ack.set_sender(command.dest());

  users_map[command.src()].push_msg_queue(ack);
  users_map[command.src()].push_msg_queue(r);
  users_map[command.dest()].push_msg_queue(r);
}


/******** AUXILIARY FUNCTIONS **********/

//This should check if the command is properly formated
//And/or all fields need for the action are filled
bool check_command(const chat::Command& command, int cmd){

  bool all_good = false;

  if(command.has_cmd() && command.cmd() == cmd){
    switch (cmd){
        case chat::Command::AUTH:
          if(command.has_src() && !command.src().empty()){
            all_good = true;
          }
          break;
        case chat::Command::SEND:
          if((command.has_src() && !command.src().empty()) && (command.has_dest() && !command.dest().empty()) && (command.has_msg() && !command.msg().empty())){
            if(users_map.find(command.src()) != users_map.end() && users_map.find(command.dest()) != users_map.end())
              all_good = true;
          }
          break;
        case chat::Command::SENDG:
          if((command.has_src() && !command.src().empty()) && (command.has_dest() && !command.dest().empty()) && (command.has_msg() && !command.msg().empty())){
            if(users_map.find(command.src()) != users_map.end() && group_map.find(command.dest()) != group_map.end())
              all_good = true;
          }
          break;
        case chat::Command::SENDF:
          if((command.has_src() && !command.src().empty()) && (command.has_dest() && !command.dest().empty()) && (command.has_file() && !command.file().empty()) &&
        (command.has_filename() && !command.filename().empty())){
            if(users_map.find(command.src()) != users_map.end() && users_map.find(command.dest()) != users_map.end())
              all_good = true;
          }
          break;
        case chat::Command::CREATEG:
          if((command.has_src() && !command.src().empty()) && (command.has_dest() && !command.dest().empty())){
            if(users_map.find(command.src()) != users_map.end() && group_map.find(command.dest()) == group_map.end())
              all_good = true;
          }
          break;
        case chat::Command::JOING:
          if((command.has_src() && !command.src().empty()) && (command.has_dest() && !command.dest().empty())){
            if(users_map.find(command.src()) != users_map.end() && group_map.find(command.dest()) != group_map.end())
              all_good = true;
          }
          break;
        case chat::Command::GAME:
          if((command.has_src() && !command.src().empty()) && (command.has_dest() && !command.dest().empty()) && command.has_x_axis() && command.has_y_axis()){
            if(tic_tac_toe.find(std::pair<std::string, std::string>(command.dest(), command.src())) == tic_tac_toe.end() &&
              tic_tac_toe.find(std::pair<std::string, std::string>(command.src(), command.dest())) == tic_tac_toe.end())
              all_good = true;
          }
          break;
        case chat::Command::GAMEA:
          if((command.has_src() && !command.src().empty()) && (command.has_dest() && !command.dest().empty()) && command.has_x_axis() && command.has_y_axis()){
            if(tic_tac_toe.find(std::pair<std::string, std::string>(command.dest(), command.src())) != tic_tac_toe.end() ||
              tic_tac_toe.find(std::pair<std::string, std::string>(command.src(), command.dest())) != tic_tac_toe.end())
              all_good = true;
          }
          break;
        case chat::Command::EXIT:
        case chat::Command::WHO:
          if(command.has_src() && !command.src().empty()){
            all_good = true;
          }
          break;
        default:
            std::cerr << "Problem parsing command" << std::endl;
    }
  }

  return all_good;
}

const chat::Command wait_on_command(const int socket, const std::string& user) {
    char *buffer;
    int recvd, sz;

    // Get size of message
    std::cout << "=========== (wait_on_command) receiving " << std::endl;
    recvd = recv(socket, &sz, sizeof(int), 0);
    if (recvd <= 0 || sz < 0) {  // Check if user disconnected without exit command
        exit(socket, user);
    }
    std::cout << "=========== (wait_on_command) msg size " << sz << std::endl;

    // Receive chat::Response
    buffer = new char[sz];
    bzero(buffer, sz);
    recvd = recv(socket, buffer, sz, 0);
    if (recvd <= 0) {  // Check if user disconnected without exit command
        exit(socket, user);
    }
    std::cout << "=========== (wait_on_command) recvd " << recvd << std::endl;

    // Parse protobuf
    chat::Command c;
    bool res = c.ParseFromArray(buffer, sz);
    /* bool res = c.ParseFromString(buffer); */
    std::cout << res << std::endl;
    delete[] buffer;

    std::cout << "=========== (wait_on_command) command received " << recvd << std::endl;
    if (!c.IsInitialized())
        std::cout << "=========== (wait_on_command) nope" << std::endl;
    c.PrintDebugString();

    return c;
}

// Send response to server through socket
void send_response(const int socket, const chat::Response& response) {

    // Send size of message
    std::cout << "=========== (send_respone) sending " << std::endl;
    response.PrintDebugString();

    int sz = response.ByteSize();
    std::cout << "=========== (send_respone) msg size " << sz << std::endl;
    send(socket, &sz, sizeof(int), 0);

    // Send message itself
    char *buffer = new char[sz];
    response.SerializeToArray(buffer, sz);
    int sent = send(socket, buffer, sz, 0);
    delete[] buffer;

    std::cout << "===========  (send_respone) sent: " << sent << std::endl;
}
