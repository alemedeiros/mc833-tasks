#include "data.h"

#include <iostream>

/**************** USER CLASS ****************/
User::User(const std::string& username, const int socket){
  _username = username;
  _socket = socket;
  //_msg_queue();
}
User::User(const User& other){
  _username = other._username;
  _socket = other._socket;
  _msg_queue = other._msg_queue;
}

User& User::operator=( const User& other ) {
      _username = other._username;
      _socket = other._socket;
      _msg_queue = other._msg_queue;
      return *this;
  }

bool User::is_online(){
  return _socket != -1;
}

const std::string& User::get_username(){
  return _username;
}

int User::get_socket(){
  return _socket;
}

void User::set_online(const int socket){
  if(socket >= 0)
    _socket = socket;
  else
      std::cerr << "invalid socket for user " << _username << std::endl;
}

void User::set_offline(){
  _socket = -1;
}

void User::push_msg_queue(const chat::Response& msg){
  std::lock_guard<std::mutex> lock(m);

  _msg_queue.push(msg);
  c.notify_one();
}

const chat::Response User::pop_msg_queue(){
  std::unique_lock<std::mutex> lock(m);

  while (_msg_queue.empty())
      c.wait(lock);

  if (!is_online()) {
      chat::Response r;
      return r;
  }

  chat::Response ret = _msg_queue.front();
  _msg_queue.pop();

  return ret;
}

bool User::queue_empty(){
  return _msg_queue.empty();
}
/**********************************************/

/**************** GROUP CLASS ****************/
Group::Group(const std::string& groupname){
  _groupname = groupname;
}
Group::Group(const Group& other){
    _groupname = other._groupname;
    _members = other._members;
}

Group& Group::operator=( const Group& other ) {
    _groupname = other._groupname;
    _members = other._members;
      return *this;
  }


const std::string& Group::get_groupname(){
  return _groupname;
}

void Group::add_member(const std::string& username){
  std::lock_guard<std::mutex> lock(addm_mutex);

  _members.push_back(username);
}

int Group::size(){
  return _members.size();
}

const std::list<std::string>& Group::get_members(){
  return _members;
}
/**********************************************/

/**************** GROUP CLASS ****************/
Board::Board(std::string X, std::string O){
    // NOTE: Could be a standard matrix, as the size is fixed
    // NOTE 2: Yeah ! but c++ does not like malloc, or putting anything in the heap for that matter
    // NOTE 3: but why not just int[3][3] ?
  _board = std::vector<std::vector<int> >(3, std::vector<int>(3));
  _X = X;
  _O = O;
  _last_player = "";
}

Board::Board(const Board& other){
  _X = other._X;
  _O = other._O;
  _last_player = other._last_player;
  _board = other._board;
}

Board& Board::operator=( const Board& other){
  _X = other._X;
  _O = other._O;
  _last_player = other._last_player;
  _board = other._board;
  return *this;
}

bool Board::allowed_move(const int x, const int y){
  return !(x < 0 || x > 2 || y < 0 || y > 2 || _board[x][y]);
}

int Board::playertoint(std::string playername){
  if(playername == _X)
    return 1;
  return 2;
}

int Board::make_move(const int x, const int y, const std::string player){
  if(allowed_move(x,y) && player != _last_player){
    _board[x][y] = playertoint(player);
    _last_player = player;
    if(win()){
      return playertoint(player);
    }
    if (draw()){
        return 3;
    }
    return 0;
  }
  return -1;
}

bool Board::win(){
  bool match = false;

  for(int rc = 0; rc < 3; rc++){
    if(_board[0][rc] != 0 && _board[0][rc] == _board[1][rc] && _board[1][rc] == _board[2][rc]){
      match = true;
      break;
    }
    if(_board[rc][0] != 0 && _board[rc][0] == _board[rc][1] && _board[rc][1] == _board[rc][2]){
      match = true;
      break;
    }
  }
  if(_board[0][0] != 0 && _board[0][0] == _board[1][1] && _board[1][1] == _board[2][2]){
    match = true;
  }
  if(_board[0][2] != 0 && _board[0][2] == _board[1][1] && _board[1][1] == _board[2][0]){
    match = true;
  }

  return match;
}

bool Board::draw(){
    for(int r = 0; r < 3; r++){
        for(int c = 0; c < 3; c++) {
            if(_board[r][c] == 0)
                return false;
        }
    }
    return true;
}

int Board::is_filled(const int x, const int y){
  return _board[x][y];
}

const std::string Board::board_string() {
  std::string b_str = "    A   B   C \n";
  int row_val = 3;
  for(int row = 0; row < 5; row++){
    if(row % 2 == 0){
      b_str += " " + std::to_string(row_val) + " ";
      row_val--;
      for(int col = 0; col < 3; col++){
        int game_row = (row == 2) ? 1 : (row == 4) ? 2 : 0;
        if(_board[game_row][col] == 1){
          b_str += " X ";
        } else if(_board[game_row][col] == 2){
          b_str += " O ";
        } else{
          b_str += "   ";
        }
        if(col < 2)
          b_str += "|";
      }
      b_str += "\n";
    }
    else{
      b_str += "   ---|---|---\n";
    }
  }
  return b_str;
}

/**********************************************/
